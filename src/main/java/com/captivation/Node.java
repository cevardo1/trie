package com.captivation;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 */


public class Node {

	private char value;
	private List<Node> children;
	
    public Node() {
    	this.value = '.';
    	this.children = new ArrayList<Node>();
	}
    
    public Node(char c) {
    	this.value = c;
	    this.children = new ArrayList<Node>();
	}    

	public List<Node> getChildren() {
		return this.children;
	}

	public void setChildren(Node child) {
		this.children.add(child);
	}

	public char getValue() {
		return this.value;
	}

	public void setValue(char value) {
		this.value = value;
	}
}
