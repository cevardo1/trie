package com.captivation;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Content implements Trie{
	
	private Gson content;
	
	private Root root; 
	
	public Content() throws IOException {
		
		Writer writer = new FileWriter("content.json");
		
		this.root = new Root();			
		this.content = new GsonBuilder().create();
		this.content.toJson(new Root(), writer);
		
		writer.close();
		
	}
	
	public boolean add(String word) {
		StringCharacterIterator sci = new StringCharacterIterator(word);
		
		// Recursively add character nodes
		if (StringUtils.isEmpty(word)) {
			System.out.println("wtf");
			writeRoot();
			return true;
		}
			 
		// Store first character of new word
		char key = word.charAt(0);
		Node currentNode = new Node(key);
		
		if(getRoot().getNodes().add(currentNode)) {
			if (CharacterIterator.DONE != sci.next()) {
				add(word.substring(1));
			}
		} 
        return false;
	}
	
	public boolean contains(String word) {
		return getRoot().getDictionary().contains(word);

	}

	public List<String> search(String prefix) {

		List<String> found = new ArrayList<String>();
		for (String s : getRoot().getDictionary()) {
			if (s.startsWith(prefix)) {
				found.add(s);
			}
		}
		return found;
	}
	
	public void writeRoot () {
		
		System.out.println();
		try {
			Writer writer= new FileWriter("content.json"); 
			writer.write(this.content.toJson(getRoot()));
			writer.close();
		
		} catch (IOException e) {
			e.printStackTrace();
		
		}
	}
	
	public Root getRoot() {
		try {
			Reader reader = new FileReader("content.json");
			this.root = this.content.fromJson(reader, Root.class);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return this.root;
	}
	
	public List<Node> getNodes() {
		List<Node> nodes = null;
		try {
			Reader reader = new FileReader("content.json");
			nodes = (List<Node>) this.content.fromJson(reader, Node.class);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return nodes;
	}
	
	public Dictionary getDictionary() {
		Dictionary dictionary = null;
		
		try {
			Reader reader = new FileReader("content.json");
			dictionary = this.content.fromJson(reader, Dictionary.class);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dictionary;
	}
	
	public String printContent() {
		return this.content.toJson(getRoot());

	}
	
	public String printNodes() {
		return this.content.toJson(getRoot());

	}

	public String printDictionary() {
		return this.content.toJson(getRoot());

	}
}
