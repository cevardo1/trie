package com.captivation;

import java.util.ArrayList;
import java.util.List;

// Operates as the root node of a Json Object
public class Root {

	List<Node> nodes; // Manages all ROOT nodes
	List<String> dictionary; // Manages all completed words in dictionary

	public Root() {
		this.nodes = new ArrayList<Node>();
		this.dictionary = new ArrayList<String>();
	}

	public List<Node> getNodes() {
		return nodes;
	}

	public void setNodes(Node rootNode) {
		this.nodes.add(rootNode);
	}

	public List<String> getDictionary() {
		return this.dictionary;
	}

	public void setDictionary(String word) {
		this.dictionary.add(word);
	}
}
