package com.captivation;

import java.util.ArrayList;
import java.util.List;

public class Dictionary implements Trie{

	private List<String> words;
	
	public Dictionary() {
		this.words = new ArrayList<String>();
	}
	
	public List<String> getWords() {
		return this.words;
	}

	public boolean add(String word) {		
		return this.words.add(word);
	}

	public boolean contains(String word) {
		return this.words.contains(word);
	}

	public List<String> search(String prefix) {
		
		List<String> found = new ArrayList<String>();
		
		for (String find : this.words) {
			if(find.contains(prefix)) {
				found.add(find);
			}
		}
		
		return found;
	}
	
	public String printDictionary() {
		return this.words.toString();
	}
}
