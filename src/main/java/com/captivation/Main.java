package com.captivation;
/**
 * 
 */

import java.io.IOException;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

/**
 * @author hamSLAMwich
 *
 */

public class Main {

	public static void main(String[] args) throws IOException {
		

		Scanner scanner = new Scanner(System.in);
		Content content = new Content();
		String word;
		String input;
		int option = 0;
		
		try {
			
			do {
				
				System.out.println("Add Words(Type stuff)  Check Dictionary(1)  Prefix Search(2)  Exit(3)");
				System.out.print("Option: ");
				
				input = scanner.nextLine();
				word = input;
				
				if (input.equals("3")) {
					option = 3;
					
				} else if (StringUtils.isNumeric(input)) {
					option = Integer.parseInt(input);
				
				} else {
					// Text
					option = 0;
				}
				
				switch (option) {
				
					case 1: // Dictionary
						System.out.println("Print Dictionary: ");
						content.printDictionary();
						
						break;
						
					case 2:
						System.out.println("Prefix list: ");
						content.search(word);
						
						break;
					
					case 3:
						System.out.println("Exiting...");
						break;
						
					default:
						// Reprompt if janky
						if (!StringUtils.isAlpha(word) || !StringUtils.isAllLowerCase(word) || StringUtils.isEmpty(word)) {
							System.out.println(" - Invalid entry: lowercase alphabetical characters only - ");
											
						} else {						
							if (content.add(word)) {
								content.getRoot().getDictionary().add(word);
								System.out.println("Dictionary updated!");
							}
						}
						break;
				}				
							
			} while (option != 3);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			scanner.close();
		}
		
	}
	
}
